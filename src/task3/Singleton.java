package task3;

public class Singleton {
    private static Singleton uniqueInstance;

    private Singleton() {

    }

    public static Singleton getInstance(){
        if(uniqueInstance == null){
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }
    //другие методы

   /**
    * Паттерн Singleton имеет проблему многопоточного доступа
    * имеет 3 пути решения:
    * 1. Объявить метод synchronized
    * public static synchronized Singleton getInstance()
    * минус: замедляет выполнение программы
    *
    * 2. Создавать экземпляр заранее
    * private static Singleton uniqueInstance = new Singleton();
    * минус: класс создан, но ещё долго может не использоваться
    *
    * 3. private volatile static Singleton uniqueInstance;
    * минус: недоступен в Java 1.4
    */
}
