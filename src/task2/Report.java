package task2;

import java.util.ArrayList;
import java.util.List;

class Report {
    private List<ReportItem> items; // Отчетные данные

    // расчет отчетных данных
    public void calculate() {
        items = new ArrayList<>();
        items.add(new ReportItem("First", 5f));
        items.add(new ReportItem("Second", 6f));
    }

    public void output() {
        OutReport reportPrint = new PrintReport();
        OutReport reportDesktop = new DesktopReport();

        reportPrint.output(items);
        reportDesktop.output(items);
    }
}
