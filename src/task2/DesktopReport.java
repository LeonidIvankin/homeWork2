package task2;

import java.util.List;

class DesktopReport extends OutReport{
    @Override
    public void output(List<ReportItem> items) {
        System.out.println("Output to desktop");
        for (ReportItem item : items) {
            System.out.format("desktop %s - %f \n\r", item.getDescription(), item.getAmount());
        }
    }
}
