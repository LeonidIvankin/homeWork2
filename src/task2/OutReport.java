package task2;

import java.util.List;

public abstract class OutReport {
    abstract void output(List<ReportItem> items);
}
