package task2;

class Programm {
    public static void main(String[] args) {
        //применил паттер Шаблонный метод
        Report report = new Report();
        report.calculate();
        report.output();
    }
}
