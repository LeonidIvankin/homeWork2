package task1;

public class MeteoStation{
    public static void main(String[] args){
        //применил паттерн Адаптер
        MeteoStore meteoDb = new MeteoStore();

        MeteoSensor ms200_1 = new MS200(1);
        meteoDb.save(ms200_1);

        // Здесь надо вызвать метод getData у класса task1.ST500Info.
        // Полученные данные отправить в метод save объекта meteoDb

        ST500Info st500Info = new ST500Info();
        MeteoSensorAdapter meteoSensorAdapter = new MeteoSensorAdapter(st500Info.getData());
        meteoDb.save(meteoSensorAdapter);

    }
}